package com.babestudios.companyinfouk.persons

import com.arkivanov.mvikotlin.extensions.coroutines.states
import com.arkivanov.mvikotlin.main.store.DefaultStoreFactory
import com.babestudios.base.kotlin.ext.test
import com.babestudios.companyinfouk.shared.domain.api.CompaniesRepository
import com.babestudios.companyinfouk.shared.domain.model.persons.PersonsResponse
import com.babestudios.companyinfouk.shared.screen.persons.PersonsExecutor
import com.babestudios.companyinfouk.shared.screen.persons.PersonsStore
import com.babestudios.companyinfouk.shared.screen.persons.PersonsStoreFactory
import com.github.michaelbull.result.Ok
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import org.junit.Before
import org.junit.Test

class PersonsTest {

	private val companiesHouseRepository = mockk<CompaniesRepository>()

	private lateinit var filingHistoryExecutor: PersonsExecutor

	private lateinit var personsStore: PersonsStore

	private val testCoroutineDispatcher = Dispatchers.Unconfined

	@Before
	fun setUp() {
		coEvery {
			companiesHouseRepository.logScreenView(any())
		} answers { }

		coEvery {
			companiesHouseRepository.getPersons("123", "0")
		} answers { Ok(PersonsResponse()) }

		filingHistoryExecutor = PersonsExecutor(
			companiesHouseRepository,
			testCoroutineDispatcher,
			testCoroutineDispatcher
		)

		personsStore =
			PersonsStoreFactory(DefaultStoreFactory(), filingHistoryExecutor).create(
				companyId = "123"
			)
	}

	@Test
	fun `when get persons then repo get persons is called`() {
		val states = personsStore.states.test()

		states.last().personsResponse shouldBe PersonsResponse()
		coVerify(exactly = 1) { companiesHouseRepository.getPersons("123", "0") }
	}

	@Test
	fun `when load more persons then repo load more persons is called`() {
		val states = personsStore.states.test()
		personsStore.accept(PersonsStore.Intent.LoadMorePersons)

		states.last().personsResponse shouldBe PersonsResponse()
		coVerify(exactly = 1) { companiesHouseRepository.getPersons("123", "0") }
	}

}
