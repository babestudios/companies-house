package com.babestudios.companyinfouk.insolvencies

import com.arkivanov.mvikotlin.extensions.coroutines.states
import com.arkivanov.mvikotlin.main.store.DefaultStoreFactory
import com.babestudios.base.kotlin.ext.test
import com.babestudios.companyinfouk.shared.domain.api.CompaniesRepository
import com.babestudios.companyinfouk.shared.domain.model.insolvency.Insolvency
import com.babestudios.companyinfouk.shared.screen.insolvencies.InsolvenciesExecutor
import com.babestudios.companyinfouk.shared.screen.insolvencies.InsolvenciesStore
import com.babestudios.companyinfouk.shared.screen.insolvencies.InsolvenciesStoreFactory
import com.github.michaelbull.result.Ok
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import org.junit.Before
import org.junit.Test

class InsolvenciesTest {

	private val companiesHouseRepository = mockk<CompaniesRepository>()

	private lateinit var insolvenciesExecutor: InsolvenciesExecutor

	private lateinit var insolvenciesStore: InsolvenciesStore

	private val testCoroutineDispatcher = Dispatchers.Unconfined

	@Before
	fun setUp() {
		coEvery {
			companiesHouseRepository.logScreenView(any())
		} answers { }

		coEvery {
			companiesHouseRepository.getInsolvency("123")
		} answers { Ok(Insolvency(cases = emptyList())) }

		insolvenciesExecutor = InsolvenciesExecutor(
			companiesHouseRepository,
			testCoroutineDispatcher,
			testCoroutineDispatcher
		)

		insolvenciesStore =
			InsolvenciesStoreFactory(DefaultStoreFactory(), insolvenciesExecutor).create(
				selectedCompanyId = "123"
			)
	}

	@Test
	fun `when get insolvencies then repo get insolvencies is called`() {
		val states = insolvenciesStore.states.test()

		coVerify(exactly = 1) { companiesHouseRepository.getInsolvency("123") }
		states.last().insolvency.cases shouldBe emptyList()
	}

}
